<!DOCTYPE html>
    
    <?php

        session_start();
        require('database.php');
?>
<head>
<meta charset="utf-8"/>
<title>User Profile</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
        td.title{
            font-size: 150%;
            font-weight: 200;
            color: blue;
        }
        table{
            margin: 0px 100px;
        }
	</style>                                           
</head>
<body><div id="main">

	<h1>
		Welcome to your profile,
        <?php
            if(isset($_SESSION['username'])) {
                $username = $_SESSION['username'];
                echo htmlentities($username);
           } else {
             header("Location: logincheck.php");
           }
           if(isset($_SESSION['user_id'])) {
                $user_id = $_SESSION['user_id'];
           } else {
             header("Location: logincheck.php");
           }
        ?>
		<br>
	</h1>
	<a href="main.php">Back to Main</a>
	<br>
    <a href="logout.php">Logout</a>
	<br>
    <br>
	<hr>
	
	<h2>
        Your stories:
    </h2>
    
     <table>
        <tr>
            <th>
               Score
            </th>
            <th>
                Title
            </th>
            <th>
                View Comments
            </th>
            <th>
            </th>
            <th>
            </th>

            
        </tr>
    
    
    
    <!-- Put a table of stories here. -->
    
    <?php
    
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT id, name, link, score FROM stories WHERE poster_id = ? ORDER BY score desc");
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
    }
    
     // Bind the parameter
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($id, $name, $link, $score);
    
    
    
    while($stmt->fetch()) {
      //Build a table of stories
        
         echo '<tr> ';
                 echo '<td>';
                    echo htmlentities($score);
                 echo '</td>';
                 
                 echo '<td> ';
                    echo '<a href="';
                    echo htmlentities($link);
                    echo '">';
                    echo htmlentities($name);
                    echo '</a>';
                    
                    
                 echo '</td>';
                 
                 echo '<td> ';
                    echo '<form action = "storyview.php" method = "get">';
                        echo '<input type = "hidden" name = "story_id" value = "';
                            echo htmlentities($id);
                        echo '" />';
                        echo '<input type = "submit" value = "View" />';
                    echo '</form>';
                 echo '</td>';
                 
                 
                 // If user is logged in, allow upvoting, downvoting
                 if(isset($_SESSION['username'])) {
                    
                    // If the logged in user posted, allow editing and deletion
                    
                    if($_SESSION['username'] == $username) {
                        echo '<td> ';
                       echo '<form action = "editStory.php" method = "post">';
                           echo '<input type = "hidden" name = "story_id" value = "';
                               echo htmlentities($id);
                           echo '" />';
                           echo '<input type = "submit" value = "Edit" />';
                       echo '</form>';
                    echo '</td>';
                    
                    echo '<td> ';
                       echo '<form action = "deleteStory.php" method = "post">';
                           echo '<input type = "hidden" name = "story_id" value = "';
                               echo htmlentities($id);
                           echo '" />';
                           echo '<input type = "submit" value = "Delete" />';
                       echo '</form>';
                    echo '</td>';
                    }
                 } else {
                    echo '<td>';
                    echo '</td>';
                    echo '<td>';
                    echo '</td>';
                 }
                 
            echo '</tr>';
        
        
        
        
    }
    $stmt->close();
    
    
    ?>
    
    </table>
	
	<br><hr><br>
    <!-- BUILD A COMMENTS TABLE -->
    <h2>
        Your comments:
    </h2>
    <table>
<?php
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT comments.id, comments.content, comments.score, stories.name, stories.id FROM comments JOIN stories ON (comments.story_id = stories.id) WHERE comments.poster_id = ? ORDER BY comments.score desc");
 
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
   }

    // Bind the parameter
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($comment_id, $comment_content, $comment_score, $story_name, $story_id);

    while($stmt->fetch()) {
      //Build a table of comments
        
        
        echo '<tr>';
            echo '<td class="title" colspan = "4">';
                echo htmlentities($story_name);
            echo '</td>';
        echo '</tr>';
         echo '<tr> ';
                 echo '<td>';
                    echo htmlentities($comment_score);
                 echo '</td>';
                 
                 echo '<td> ';
                    echo htmlentities($comment_content);
                 echo '</td>';
                 
                 
                 if(isset($_SESSION['username']) && $_SESSION['username'] == $username) {
                        echo '<td> ';
                       echo '<form action = "editComment.php" method = "post">';
                        echo '<input type = "hidden" name = "comment_id" value = "';
                            echo htmlentities($comment_id);
                        echo '" />';
                        echo '<input type = "hidden" name = "story_id" value = "';
                            echo htmlentities($story_id);
                        echo '" />';
                           echo '<input type = "submit" value = "Edit" />';
                       echo '</form>';
                    echo '</td>';
                    
                    echo '<td> ';
                       echo '<form action = "deleteComment.php" method = "post">';
                           echo '<input type = "hidden" name = "comment_id" value = "';
                            echo htmlentities($comment_id);
                        echo '" />';
                        echo '<input type = "hidden" name = "story_id" value = "';
                            echo htmlentities($story_id);
                        echo '" />';
                           echo '<input type = "submit" value = "Delete" />';
                       echo '</form>';
                    echo '</td>';
                    } else {
                    echo '<td>';
                    echo '</td>';
                    echo '<td>';
                    echo '</td>';
                 }
                 
            echo '</tr>';
        
    }
    
    
    $stmt->close();
    
?>

</table>
    
    
    
<br><hr><br>

<!-- Obtain total story and comment scores -->

<?php
// Use a prepared statement

    
    $stmt = $mysqli->prepare("SELECT SUM(score) as total_score FROM stories WHERE poster_id = ?");
 
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
   }

    // Bind the parameter
    $stmt->bind_param('s', $user_id);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($total_story_score);
    $stmt->fetch();
    $stmt->close();
    
    $stmt = $mysqli->prepare("SELECT SUM(score) as total_score FROM comments WHERE poster_id = ?");
 
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
   }

    // Bind the parameter
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($total_comment_score);
    $stmt->fetch();
    $stmt->close();
     
    if($total_story_score == null) {
        $total_story_score = 0;
    }
    if($total_comment_score == null) {
        $total_comment_score = 0;
    }
    
?>

<h2>
    <?php echo $total_story_score; ?>  
</h2>
<p>
    story score
</p>
<br>
<h2>
    <?php echo $total_comment_score; ?>  
</h2>
<p>
    comment score
</p>	
 
</div></body>
</html>