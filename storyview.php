<!DOCTYPE html>


<?php

    require 'database.php';
    
	session_start();
	
?>

<head>
<meta charset="utf-8"/>
<title>Story Page</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

<?php
        
            if(isset($_SESSION['username'])) {
             echo ' Logged in as ';
             echo htmlentities($_SESSION['username']);
           } else {
             echo ' You are not logged in.';
           }
        
?>
    <form action = "main.php">
        <input type = "submit" value = "Back to Main" />
    </form> <br>


<?php

	$story_id = (int) $_GET['story_id'];
    
    
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT stories.name, stories.link, stories.score, stories.content, users.username FROM stories JOIN users ON (users.id = stories.poster_id) WHERE stories.id = ?");
 
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
   }

    // Bind the parameter
    $stmt->bind_param('s', $story_id);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($name, $link, $score, $content, $username);
    $stmt->fetch();
    $stmt->close();
    
?>

<h1>
    <a href <?php echo "="; echo htmlentities($link);?> >
        <?php echo htmlentities($name);?>
    </a>
</h1>
<br>
<p>
    Posted by: <?php echo htmlentities($username);?>
</p>
<p>
    <?php echo htmlentities($content)?>
</p>
<hr>

<?php
    if(isset($_SESSION['username'])) {
        echo '<h3>Write a comment below:</h3>';
         echo '<form action="addcomment.php" method="POST">';
         echo '<input type = "hidden" name = "story_id" value = "';
            echo htmlentities($story_id);
        echo '" />';
        echo '<textarea name = "content" cols = "40" rows = "10"></textarea> <br>';
        
         echo '<input type="hidden" name="token" value="';
                           echo $_SESSION['token'];
                           echo '" />';

        echo '<input type="submit" name="Comment" value="Post"/>';
        echo '</form>';
    }
?>

<table>
<?php
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT comments.id, comments.content, comments.score, users.username FROM comments JOIN users ON (users.id = comments.poster_id) WHERE comments.story_id = ? ORDER BY comments.score desc");
 
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
   }

    // Bind the parameter
    $stmt->bind_param('s', $story_id);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($comment_id, $comment_content, $comment_score, $comment_username);

    while($stmt->fetch()) {
      //Build a table using all this info
        
         echo '<tr> ';
                 echo '<td>';
                    echo htmlentities($comment_score);
                 echo '</td>';
                 
                 echo '<td> ';
                    echo htmlentities($comment_username);                
                 echo '</td>';
                 
                 echo '<td> ';
                    echo htmlentities($comment_content);
                 echo '</td>';
        if(isset($_SESSION['username']))     {
                 echo '<td> ';
                    echo '<form action = "commentVote.php" method = "post">';
                        echo '<input type = "hidden" name = "comment_id" value = "';
                            echo htmlentities($comment_id);
                        echo '" />';
                        echo '<input type = "hidden" name = "story_id" value = "';
                            echo htmlentities($story_id);
                        echo '" />';
                        echo '<input type = "hidden" name = "type_of_vote" value = "';
                               echo 'upvote';
                           echo '" />';
                        echo '<input type = "submit" value = "Upvote" />';
                    echo '</form>';
                 echo '</td>';
                 
                 echo '<td> ';
                    echo '<form action = "commentVote.php" method = "post">';
                        echo '<input type = "hidden" name = "comment_id" value = "';
                            echo htmlentities($comment_id);
                        echo '" />';
                        echo '<input type = "hidden" name = "story_id" value = "';
                            echo htmlentities($story_id);
                        echo '" />';
                        echo '<input type = "hidden" name = "type_of_vote" value = "';
                               echo 'downvote';
                           echo '" />';
                        echo '<input type = "submit" value = "Downvote" />';
                    echo '</form>';
                 echo '</td>';
    }
                 if(isset($_SESSION['username']) && $_SESSION['username'] == $comment_username) {
                        echo '<td> ';
                       echo '<form action = "editComment.php" method = "post">';
                        echo '<input type = "hidden" name = "comment_id" value = "';
                            echo htmlentities($comment_id);
                        echo '" />';
                        echo '<input type = "hidden" name = "story_id" value = "';
                            echo htmlentities($story_id);
                        echo '" />';
                        echo '<input type="hidden" name="token" value="';
                           echo $_SESSION['token'];
                           echo '" />';
                           echo '<input type = "submit" value = "Edit" />';
                       echo '</form>';
                    echo '</td>';
                    
                    echo '<td> ';
                       echo '<form action = "deleteComment.php" method = "post">';
                           echo '<input type = "hidden" name = "comment_id" value = "';
                            echo htmlentities($comment_id);
                        echo '" />';
                        echo '<input type = "hidden" name = "story_id" value = "';
                            echo htmlentities($story_id);
                        echo '" />';
                        echo '<input type="hidden" name="token" value="';
                           echo $_SESSION['token'];
                           echo '" />';
                           echo '<input type = "submit" value = "Delete" />';
                       echo '</form>';
                    echo '</td>';
                    }
                 
            echo '</tr>';
        
    }
    
    
    $stmt->close();
    
?>

</table>
    
</div></body>
</html>