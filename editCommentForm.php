<?php

    session_start();
    require 'database.php';
    
?>
<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Edit Failed</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

<br>
<form action = "main.php">
        <input type = "submit" value = "Back to Main Page" />
</form> <br> <br> 

<p>

<?php
    
	
	if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}
   
	//Check to see if username is valid
	
    
    if(!isset($_SESSION['username'])) {
        echo "You must be logged in to access this page";
        exit;
    }
    
    //Parse the link and content, if they exist
    $content = null;        
    
    
    if(isset($_POST['content'])) {
        $content = (string) htmlentities($_POST['content']);
        if(strlen($content) > 65535) {
        echo "The content is too large! 65535 characters max!";
        exit;
    }
    }
    
    if(isset($_POST['story_id'])) {
        $story_id = $_POST['story_id'];
    } else {
        echo "No story associated with this edit";
        exit;
    }
    
    if(isset($_POST['comment_id'])) {
        $comment_id = $_POST['comment_id'];
    } else {
        echo "No comment associated with this edit";
        exit;
    }
    
    //Insert into database
    
 // Use a prepared statement
    $stmt = $mysqli->prepare("UPDATE comments SET content = ? WHERE id = ?");
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
    }
 
    // Bind the parameter
    $stmt->bind_param('si', $content, $comment_id);
    $stmt->execute();
    $stmt->close();

	header("Location: storyview.php?story_id=".$story_id);   
    exit;
   
    
    
?>



</p>


</div></body>
</html>