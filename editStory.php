<!DOCTYPE html>
    
<?php

        session_start();
        require('database.php');
?>

<head>
<meta charset="utf-8"/>
<title>Edit Story</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

    <?php
        if(!isset($_SESSION['username'])) {
            header("Location: logincheck.php");
            exit;
        }
    ?>
    
    <?php

    if(isset($_POST['story_id'])) {
        $story_id = (int) $_POST['story_id'];
    } else {
        echo "You must choose a story to edit";
        exit;
    }
    
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT stories.name, stories.link, stories.content FROM stories WHERE stories.id = ?");
 
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
   }

    // Bind the parameter
    $stmt->bind_param('s', $story_id);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($name, $link, $content);
    $stmt->fetch();
    $stmt->close();
    $link = substr($link, 7);

?>


    <form action="editStoryForm.php" method="POST">
		<h2>Edit story</h2> <br> 
		<label>Title (100 char max) </label> <br>
		<input type="text" name="name" value = "<?php echo htmlentities($name); ?>" /> <br> <br> 
		<label>Link (optional. 200 char max)</label>
        <br>
        <label>http://</label>
		<input type="text" name="link" value = "<?php echo htmlentities($link); ?>"/> <br> <br> 

		<label>Content (optional) </label>
        <br>
		<textarea name = "content" cols = "40" rows = "10" ><?php echo htmlentities($content); ?></textarea> <br>
		
        <input type = "hidden" name = "story_id" value = "<?php echo htmlentities($story_id); ?>" />
		
	<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />

		<input type="submit" name="Post" value="Submit Edits"/>
	</form>
    
    <br>
    
    <a href="main.php">Cancel</a>
</div></body>
</html>