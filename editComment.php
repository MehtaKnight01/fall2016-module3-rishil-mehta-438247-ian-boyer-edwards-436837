<!DOCTYPE html>
    
<?php

        session_start();
        require('database.php');
?>

<head>
<meta charset="utf-8"/>
<title>Edit Comment</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">



    <?php
        if(!isset($_SESSION['username'])) {
            header("Location: logincheck.php");
            exit;
        }
    ?>
    
    <?php

    
    if(isset($_POST['comment_id'])) {
        $comment_id = (int) $_POST['comment_id'];
    } else {
        echo "You must choose a comment to edit";
        exit;
    }
    
    
    if(isset($_POST['story_id'])) {
        $story_id = (int) $_POST['story_id'];
    } else {
        echo "You must choose a story to edit";
        exit;
    }
    
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT comments.content FROM comments WHERE comments.id = ?");
 
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
   }

    // Bind the parameter
    $stmt->bind_param('s', $comment_id);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($content);
    $stmt->fetch();
    $stmt->close();

?>


    <?php
    if(isset($_SESSION['username'])) {
        echo '<h3>Edit comment</h3>';
         echo '<form action="editCommentForm.php" method="POST">';
         echo '<input type = "hidden" name = "story_id" value = "';
            echo htmlentities($story_id);
        echo '" />';
        echo '<input type = "hidden" name = "comment_id" value = "';
            echo htmlentities($comment_id);
        echo '" />';
        echo '<textarea name = "content" cols = "40" rows = "10">';
        echo htmlentities($content);
        echo'</textarea> <br>';

		echo '<input type="hidden" name="token" value="';
	echo $_SESSION['token'];
	echo '" />';
			   
        echo '<input type="submit" name="Comment" value="Post"/>';
        echo '</form>';
    }
?>
    <br>
    
    <a href="storyview.php?story_id= <?php echo $story_id?>">Cancel</a>
</div></body>
</html>