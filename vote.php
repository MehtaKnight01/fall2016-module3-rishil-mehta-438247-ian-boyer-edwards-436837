<?php

    session_start();
    require 'database.php';
    
?>
<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Vote Failed</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

<br>
<form action = "main.php">
        <input type = "submit" value = "Back to Main Page" />
</form> <br> <br> 

<p>

<?php
    
   
	//Check to see if username is valid
	
    
    if(!isset($_SESSION['username'])) {
        echo "You must be logged in to access this page";
        exit;
    }
    
    if(isset($_POST['type_of_vote']) && $_POST['type_of_vote'] == "upvote") {
        $increment = 1;
    }
	if (isset($_POST['type_of_vote'])  && $_POST['type_of_vote'] == "downvote") {
		$increment = -1;
	}

    
	if(isset($_POST['story_id'])) {
		$story_id = $_POST['story_id'];
	} else {
		echo 'No story associated with this voting';
		exit;
	}
    
    //Update database
    
	// Use a prepared statement
    $stmt = $mysqli->prepare("UPDATE stories SET score = score + ? WHERE id = ?");
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
    }
 
    // Bind the parameter
    $stmt->bind_param('is', $increment, $story_id);
    $stmt->execute();
    $stmt->close();

	
	header("Location: main.php");   
    
    
?>

</p>
<form action="storyview.php" method="post">
        <?php
            echo '<input type = "hidden" name = "story_id" value = "';
                echo htmlentities($story_id);
            echo '" />';
        ?>
		<input type="submit" name="Back to story" value="Back to story"/>
</form>


</div></body>
</html>