<!DOCTYPE html>
    
<?php

        session_start();
        require('database.php');
?>

<head>
<meta charset="utf-8"/>
<title>Main</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

    <p> Welcome to our site! 
        <?php
            if(isset($_SESSION['username'])) {
             echo ' Logged in as ';
             echo htmlentities($_SESSION['username']);
           } else {
             echo ' You are not logged in.';
           }
        ?>
    </p>
    <?php
    if(isset($_SESSION['username'])) {
        echo '<a href="profile.php">User Profile</a><br><br>';
        echo '<a href="upload.php">Post a new story!</a><br><br>';
    }
?>
    
    
    <form action = "index.html">
        <input type = "submit" value = "Back to Login" />
    </form> 
    
    
    <?php
    if(isset($_SESSION['username'])) {
        echo '<a href= "logout.php">Log Out</a>';
    }
?>
    
    
    <br>    <hr>
    
   <form action="search.php" method="post">
		<label>Search for a specific title:</label>
		<input type="text" name="title" /> <input type="submit" name="Go" /> 
		
	</form>
    <br>
    
    
    <table style="margin-left: 100px; margin-right: 100px;">
        <tr>
            <th>
               Score
            </th>
            <th>
                Title
            </th>
            <th>
                View Comments
            </th>
            <th>
                Posted By:
            </th>
            <th>
            </th>
            <th>
            </th>
             <th>
            </th>
            <th>
            </th>
        </tr>
    
    
    
    <!-- Put a table of stories here. -->
    
    <?php
    
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT stories.id, stories.name, stories.link, stories.score, users.username FROM stories JOIN users ON (users.id = stories.poster_id) ORDER BY stories.score desc");
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
    }
 
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($id, $name, $link, $score, $username);
    
    
    
    while($stmt->fetch()) {
      //Build a table using all this info
        
         echo '<tr> ';
                 echo '<td>';
                    echo htmlentities($score);
                 echo '</td>';
                 
                 echo '<td> ';
                    echo '<a href="';
                    echo htmlentities($link);
                    echo '">';
                    echo htmlentities($name);
                    echo '</a>';
                    
                    
                 echo '</td>';
                 
                 echo '<td> ';
                    echo '<form action = "storyview.php" method = "get">';
                        echo '<input type = "hidden" name = "story_id" value = "';
                            echo htmlentities($id);
                        echo '" />';
                        echo '<input type = "submit" value = "View" />';
                    echo '</form>';
                 echo '</td>';
                 
                 echo '<td> ';
                    echo htmlentities($username);
                 echo '</td>';
                 
                 // If user is logged in, allow upvoting, downvoting
                 if(isset($_SESSION['username'])) {
                    echo '<td> ';
                       echo '<form action = "vote.php" method = "post">';
                           echo '<input type = "hidden" name = "story_id" value = "';
                               echo htmlentities($id);
                           echo '" />';
                           echo '<input type = "hidden" name = "type_of_vote" value = "';
                               echo 'upvote';
                           echo '" />';
                           echo '<input type = "submit" value = "Upvote" />';
                       echo '</form>';
                    echo '</td>';
                    
                    echo '<td> ';
                       echo '<form action = "vote.php" method = "post">';
                           echo '<input type = "hidden" name = "story_id" value = "';
                               echo htmlentities($id);
                           echo '" />';
                           echo '<input type = "hidden" name = "type_of_vote" value = "';
                               echo 'downvote';
                           echo '" />';
                           echo '<input type = "submit" value = "Downvote" />';
                       echo '</form>';
                    echo '</td>';
                    
                    // If the logged in user posted, allow editing and deletion
                    
                    if($_SESSION['username'] == $username) {
                        echo '<td> ';
                       echo '<form action = "editStory.php" method = "post">';
                           echo '<input type = "hidden" name = "story_id" value = "';
                               echo htmlentities($id);
                           echo '" />';
                           echo '<input type="hidden" name="token" value="';
                           echo $_SESSION['token'];
                           echo '" />';
                           echo '<input type = "submit" value = "Edit" />';
                       echo '</form>';
                    echo '</td>';
                    
                    echo '<td> ';
                       echo '<form action = "deleteStory.php" method = "post">';
                           echo '<input type = "hidden" name = "story_id" value = "';
                               echo htmlentities($id);
                           echo '" />';
                           echo '<input type="hidden" name="token" value="';
                           echo $_SESSION['token'];
                           echo '" />';
                           echo '<input type = "submit" value = "Delete" />';
                       echo '</form>';
                    echo '</td>';
                    } else {
                        echo '<td>';
                        echo '</td>';
                        echo '<td>';
                        echo '</td>';
                    }
                 } else {
                    echo '<td>';
                    echo '</td>';
                    echo '<td>';
                    echo '</td>';
                    echo '<td>';
                    echo '</td>';
                    echo '<td>';
                    echo '</td>';
                 }
                 
            echo '</tr>';
        
        
        
        
    }
    $stmt->close();
    
    
    ?>
    
    </table>
    
</div></body>
</html>