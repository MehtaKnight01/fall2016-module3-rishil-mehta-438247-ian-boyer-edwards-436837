<?php

    session_start();
    require 'database.php';
    
?>
<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Search Failed</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

<br>
<form action = "main.php">
        <input type = "submit" value = "Back to Main Page" />
</form> <br> <br> 

<p>

<?php
    
   
	
    
    if(isset($_POST['title'])) {
        $title = (string) $_POST['title'];
    } else {
        echo "You must input a valid string";
    }
    
    
    if( !preg_match('/^[\w_\ \?\!\.\,\:\-]+$/', $title) ){
		echo "The title contains invalid characters.";
		exit;
	}
    
    if(strlen($title) > 100) {
       echo "The title was too long to be processed.";
        exit;
    }
    
  
    
    
    //Insert into database
    
 // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT id FROM stories WHERE name = ?");
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
    }
 
    // Bind the parameter
    $stmt->bind_param('s', $title);
    $stmt->execute();
    // Bind the results
    $stmt->bind_result($story_id);
    $stmt->fetch();
    $stmt->close();

    if($story_id != null) {
        header("Location: storyview.php?story_id=".$story_id);
    } else {
        echo "Could not find a story called ";
        echo $title;
    }
   
    
    
?>

</p>



</div></body>
</html>