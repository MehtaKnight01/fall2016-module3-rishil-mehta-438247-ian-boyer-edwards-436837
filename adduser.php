<?php

	session_start();
    require 'database.php';
    
?>
<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Add User Failed</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

<br>
<form action = "index.html">
        <input type = "submit" value = "Back to Login" />
</form> <br> <br> 

<p>

<?php
	//Check to see if username is valid and does not currently exist in users.txt
	
	if(!isset($_POST['new_username']) || !isset($_POST['new_password']) || !isset($_POST['repeat_new_password'])) {
		echo "One or more lines were not completed";
		exit;
	}
    

    $new_username = (string) $_POST['new_username'];
    
    if(strlen($new_username) == 0) {
        echo "Please enter a username";
        exit;
    }
    
    if(strlen($new_username) > 20) {
        echo "The username is more than 20 characters, please try again";
        exit;
    }
    
    if(strlen($_POST['new_password']) < 8) {
        echo "The password isn't long enough, please try again";
        exit;
    }
    
    if( !preg_match('/^[\w_\-]+$/', $new_username) ){
		echo "Invalid username. Please try again.";
		exit;
	}
	
	  //Check to see if password is valid syntactically
    
	if( !preg_match('/^[a-z0-9_-]+$/', (string) $_POST['new_password']) ){
		echo "Invalid characters in password";
		exit;
	}
    
    
    
    //Check to make sure first password = second password
    
    if($_POST['new_password'] != $_POST['repeat_new_password']) {
        echo "Passwords do not match";
        exit;
    }
    
    //Query database to see if username exists
    
     // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE username=?");
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
    }
 
    // Bind the parameter
    $stmt->bind_param('s', $user);
    $user = $_POST['new_username'];
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($cnt);
    $stmt->fetch();
    $stmt->close();

    if( $cnt == 0) {
        
        
        // Login succeeded!
        
        //Insert username, password into database
        
        $new_password = $_POST['new_password'];
        $new_password_hashed = crypt($new_password);
        
        $insert = $mysqli->prepare("INSERT INTO users (username, password) VALUES (?, ?)");
        
        if(!$insert){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }

        //Bind the parameter
        $insert->bind_param('ss', $new_username, $new_password_hashed);
        $insert->execute();
        $insert->close();
        
        //Obtain user id
    
        $stmt = $mysqli->prepare("SELECT id FROM users WHERE username=?");
        
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }

        //Bind the parameter
        $stmt->bind_param('s', $new_username);
        $stmt->execute();
        // Bind the results
        $stmt->bind_result($user_id);
        $stmt->fetch();
        $stmt->close();
            
        
        //Log em in
        $_SESSION['user_id'] = (string) $user_id;
        $_SESSION['username'] = $new_username;
		$_SESSION['token'] = substr(md5(rand()), 0, 10);


        header("Location: main.php");
		exit;
    }else{
        echo("The username already exists, please try a new one");
		exit;
        // Login failed; redirect back to the login screen
    }
    
   
    
    
?>

</p>


</div></body>
</html>