<?php

    session_start();
    require 'database.php';
    
?>
<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Add Failed</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

<br>
<form action = "main.php">
        <input type = "submit" value = "Back to Main Page" />
</form> <br> <br> 

<p>

<?php
    if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}
   
	//Check to see if username is valid
	
    
    if(!isset($_SESSION['username'])) {
        echo "You must be logged in to access this page";
        exit;
    }
    
    
    //Parse the title
    
    if(strlen($_POST['name']) == 0) {
        echo "You must include a title for your story!";
        exit;
    }
    
    $name = (string) $_POST['name'];
    
    if( !preg_match('/^[\w_\ \?\!\.\,\:\-]+$/', $name) ){
		echo "The title contains invalid characters.";
		exit;
	}
    
    if(strlen($name) > 100) {
       echo "The title was too long to be processed.";
        exit;
    }
    
    //Parse the link and content, if they exist
    $link = null;
    $content = null;
    
    if(strlen($_POST['link']) != 0) {
        $link = (string) $_POST['link'];
        if( !preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $link) ){
		echo "You must enter a valid link. (Do not include http:// or https://)";
		exit;    
        }
        if(strlen($link) > 200) {
        echo "The link was too long to be processed.";
        exit;
    }
    }
    
    
    
    
    
    
    if(isset($_POST['content'])) {
        $content = (string) htmlentities($_POST['content']);
        if(strlen($content) > 65535) {
        echo "The content is too large! 65535 characters max!";
        exit;
    }
    }
    
    
    $user_id = (int) $_SESSION['user_id'];
    
    
    if($link != null) {
        //Add http:// to the front of the link
    
        $link = "http://" . $link;
    
    }
    
    //Insert into database
    
 // Use a prepared statement
    $stmt = $mysqli->prepare("INSERT INTO stories (name, link, content, poster_id) VALUES (?, ?, ?, ?)");
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
    }
 
    // Bind the parameter
    $stmt->bind_param('ssss', $name, $link, $content, $user_id);
    $stmt->execute();
    $stmt->close();

    header("Location: main.php");
    exit;
   
    
    
?>



</p>


</div></body>
</html>