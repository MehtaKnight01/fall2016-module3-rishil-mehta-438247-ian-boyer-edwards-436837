<?php

    require 'database.php';
    
	session_start();
	
	//Check to see if username and password exist
	
	if(!isset($_POST['username']) || !isset($_POST['password'])) {
		echo "No user was found";
		$_SESSION["username"] = null;
		session_destroy();
		exit;
	}
    
    //Check to see if username is valid syntactically
    
	if( !preg_match('/^[\w_\-]+$/', (string) $_POST['username']) ){
		echo "Invalid username";
		$_SESSION["username"] = null;
		session_destroy();
		header("Location: failed.html");
		exit;
	}
    
    
     //Check to see if password is valid syntactically
    
	if( !preg_match('/^[a-z0-9_-]+$/', (string) $_POST['password']) ){
		echo "Invalid characters in password";
		$_SESSION["username"] = null;
		session_destroy();
		header("Location: failed.html");
		exit;
	}
    
    
    
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT COUNT(*), id, password FROM users WHERE username=?");
 
    if(!$stmt){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
   }

    // Bind the parameter
    $stmt->bind_param('s', $user);
    $user = $_POST['username'];
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($cnt, $user_id, $pwd_hash);
    $stmt->fetch();
    
    
    $pwd_guess = $_POST['password'];
    // Compare the submitted password to the actual password hash
    if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
        // Login succeeded!
        $_SESSION['user_id'] = $user_id;
        $_SESSION['username'] = (string) $_POST['username'];
		$_SESSION['token'] = substr(md5(rand()), 0, 10);
        header("Location: main.php");
		exit;
    }else{
        header("Location: failed.html");
		exit;
        // Login failed; redirect back to the login screen
    }
    
        
?>