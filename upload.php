<!DOCTYPE html>
    
<?php

        session_start();
        require('database.php');
?>

<head>
<meta charset="utf-8"/>
<title>Add Story</title>
	<style type="text/css">
		body{
			width: 760px; /* how wide to make your web page */
			background-color: teal; /* what color to make the background */
			margin: 0 auto;
			padding: 0;
			font:12px/16px Verdana, sans-serif; /* default font */
		}
		div#main{
			background-color: #FFF;
			margin: 0;
			padding: 10px;
		}
	</style>
</head>
<body><div id="main">

    <?php
        if(!isset($_SESSION['username'])) {
            header("Location: logincheck.php");
            exit;
        }
    ?>

    <form action="addstory.php" method="POST">
		<h2>Post a story</h2> <br> 
		<label>Title (100 char max) </label> <br>
		<input type="text" name="name" /> <br> <br> 
		<label>Link (optional. 200 char max)</label>
        <br>
        <label>http://</label>
		<input type="text" name="link" /> <br> <br> 

		<label>Content (optional) </label>
        <br>
		<textarea name = "content" cols = "40" rows = "10"></textarea> <br>
		<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
		
		<input type="submit" name="Post" value="Post"/>
	</form>
</div></body>
</html>