<?php

    //Destroy session, go to home page
    
    session_start();
    $_SESSION["username"] = null;
    session_destroy();
    header("Location: main.php");
    exit;
    
?>